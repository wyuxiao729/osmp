 /*   
 * Project: OSMP
 * FileName: Client.java
 * version: V1.0
 */
package com.osmp.intf.define.server;

import java.net.InetSocketAddress;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 下午4:17:00上午10:51:30
 */
public interface Client {
	void connect();
	Response sent(Request request);
	InetSocketAddress getRemoteAddress();
	void close();
}
