 /*   
 * Project: OSMP
 * FileName: NettyClientChannelInitializer.java
 * version: V1.0
 */
package com.osmp.netty.client;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.osmp.intf.define.server.Response;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 下午4:22:41上午10:51:30
 */
@Component
public class NettyClientChannelInitializer extends  ChannelInitializer<SocketChannel> {

	@Resource
	private NettyClientDispatchHandler clientDispatchHandler;
	
	
	@Override
	protected void initChannel(final SocketChannel ch) throws Exception {
		ch.pipeline().addLast(clientDispatchHandler);
	}
	
	public Response getResponse(final String messageId) {
		return clientDispatchHandler.getResponse(messageId);
	}

}
